// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

const { ipcRenderer } = require("electron")
const items = require('../items')

//Dom Nodes
let showModal=document.getElementById('show-modal'),
 closeModal=document.getElementById('close-modal'),
  modal=document.getElementById('modal'),
 addItem=document.getElementById('add-item'),
  itemUrl=document.getElementById('url')
  search=document.getElementById('search')

  //Filter items with "search"
  search.addEventListener('keyup', e =>{

    //Loop items
    Array.from(document.getElementsByClassName('read-item')).forEach(item=>{

      //Hide items that don't match search value
      let hasMatch= item.innerHTML.toLowerCase().includes(search.value)
      item.style.display= hasMatch ? 'flex' : 'none'
    })
  })

  //Navigate item selection with up/down arrows
  document.addEventListener('keydown', e =>{
    if(e.key === 'ArrowUp' || e.key === 'ArrowDown'){
      items.changeSelection(e.key)
    }
  })

  const toggleModalButtons=()=>{
    //check state of buttons
    if(addItem.disabled === true){
        addItem.disabled=false
        addItem.style.opacity=1
        addItem.innerText='Add Item'
        closeModal.style.display='inline'
    }else{
        addItem.disabled=true
        addItem.style.opacity=0.5
        addItem.innerText='Adding...'
        closeModal.style.display='none'
    }
  }

  showModal.addEventListener('click', e=>{
    modal.style.display='flex'
    itemUrl.focus()
  })

  closeModal.addEventListener('click', e=>{
    modal.style.display='none'
  })

  //handle new item
  addItem.addEventListener('click', e=>{
    if(itemUrl.value){
        console.log(itemUrl.value)

        //send new item url to main process
        ipcRenderer.send('new-item', itemUrl.value)

        //disabble buttons
        toggleModalButtons()
    }

  })

  //listen for new item from main process
  ipcRenderer.on('new-item-success',(e,newItem)=>{
    
    console.log(newItem)
    //Add new item to items node
    items.addItem(newItem,true)

     //enable buttons
     toggleModalButtons()

     //hide modal and clear value
     modal.style.display='none'
     itemUrl.value=''
  })

  //listen for keyboard submit(invece di cliccare sul bottone posso cliccare su invio)
  itemUrl.addEventListener('keyup',e=>{
    if(e.key==='Enter'){
        addItem.click()
    }
  })