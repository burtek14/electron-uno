// Modules
const {app, BrowserWindow,ipcMain} = require('electron')
const windowStateKeeper = require('electron-window-state')
const readItem=require('./readItem')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

//listen for new item request
ipcMain.on('new-item', (e,itemUrl)=>{
  console.log(itemUrl)

  //get new item and send back to the renderer
  readItem(itemUrl,item=>{
e.sender.send('new-item-success', item)
  })
})

// Create a new BrowserWindow when `app` is ready
function createWindow () {

    //win state keeper
    let state= windowStateKeeper({
        defaultWidth:500,defaultHeight:650
    })

  mainWindow = new BrowserWindow({

    //to use the persisted states(cache, local storage etc) when the main window gets created
    //we are using the default x y coordinates initially being the center of the screen but then once they change, we still want keep the new window position, hence adding the state bounds
    //here  x and y
    x: state.x, y:state.y,
    width: state.width, height: state.height,
    minWidth:350, maxWidth:650, minHeight:300, 
    webPreferences: {
      // --- !! IMPORTANT !! ---
      // Disable 'contextIsolation' to allow 'nodeIntegration'
      // 'contextIsolation' defaults to "true" as from Electron v12
      contextIsolation: false,
      nodeIntegration: true
    }
  })

  console.log('hello')

  // Load index.html into the new BrowserWindow
  mainWindow.loadFile('renderer/index.html')

  //Manage new window state
  state.manage(mainWindow)

  // Open DevTools - Remove for PRODUCTION!
  mainWindow.webContents.openDevTools();

  // Listen for window being closed
  mainWindow.on('closed',  () => {
    mainWindow = null
  })
}

// Electron `app` is ready
app.on('ready', createWindow)

// Quit when all windows are closed - (Not macOS - Darwin)
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') app.quit()
})

// When app icon is clicked and app is running, (macOS) recreate the BrowserWindow
app.on('activate', () => {
  if (mainWindow === null) createWindow()
})